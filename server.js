
const Recognize = require('./lib/Recognize');

const express = require('express')

const app = express()

app.use(express.static(__dirname + '/client'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

var port = process.env.PORT || 41119
app.listen(port, () => {
    console.log('Server started.');
})