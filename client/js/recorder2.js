(function(window) {
  var client = new BinaryClient('w'+'s://35.2'+'29.203.63'+':443');

  client.on('open', function() {
    window.Stream = client.createStream();

    window.Stream.on('data', function(data){ 
      console.log("response: " + data);
      let res = JSON.parse(data)
      if (res.redirect) {
        window.location = res.redirect
      } else if (res.openWindow) {
        window.open(res.openWindow)
        window.location = '/#' + res.recognizeResult
        window.location.reload()
      } else {
        window.location = '/#' + res.recognizeResult
        window.location.reload()
      }
    });


    if (!navigator.getUserMedia)
      navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia || navigator.msGetUserMedia;

    if (navigator.getUserMedia) {
      navigator.getUserMedia({audio:true}, success, function(e) {
        alert('Error capturing audio.');
      });
    } else alert('getUserMedia not supported in this browser.');

    var recording = false;

    window.startRecording = function() {
      recording = true;
    }

    window.stopRecording = function() {
      recording = false;
      window.Stream.end();
    }

    function success(e) {
      audioContext = window.AudioContext || window.webkitAudioContext;
      context = new audioContext();

      // the sample rate is in context.sampleRate
      audioInput = context.createMediaStreamSource(e);

      var bufferSize = 2048;
      recorder = context.createScriptProcessor(bufferSize, 1, 1);

      recorder.onaudioprocess = function(e){
        if(!recording) return;
        console.log ('recording');
        var left = e.inputBuffer.getChannelData(0);
        window.Stream.write(convertoFloat32ToInt16(left));
      }

      audioInput.connect(recorder)
      recorder.connect(context.destination); 
    }

    function convertoFloat32ToInt16(buffer) {
      var l = buffer.length;
      var buf = new Int16Array(l)

      while (l--) {
        buf[l] = buffer[l]*0xFFFF;    //convert to 16 bit
      }
      return buf.buffer
    }
  });
  client.on('error', (e) => {
    console.error('error: ' + e)
  })
  client.on('close', (a) => {
    console.error('close: ' + a)
  })
  client.on('message', (m) => {
    console.error('message: ' + m)
  })

})(this);
