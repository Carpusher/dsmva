
const Recognize = require('./lib/Recognize');

const BinaryServer = require('binaryjs').BinaryServer
const fs = require('fs-extra')
const wav = require('wav');
const HTTPS = require('https')
const QueryString = require('querystring')

const UPLOAD_FOLDER = './uploads/'
const TARGET_DSM = 'staging.deepsecurity.trendmicro.com'
const TARGET_DSM_FULL = 'https://' + TARGET_DSM + '/'


console.log('server starting...')

let dsmLogin = (cb) => {

    let postData = JSON.stringify({
        "dsCredentials": {
            "tenantName": "Mars3",
            "userName": "mars_huang@trend.com.tw",
            "password": "P@ssw0rd3"
        }
    })
    let options = {
        hostname: TARGET_DSM,
        port: 443,
        path: '/rest/authentication/login',
        method: 'POST',
        headers: {
             'Content-Type': 'application/json'
           }
    }
    let req = HTTPS.request(options, (res) => {
      
        res.on('data', (d) => {
            console.log('login success, sid: ' + d)
            cb(d)
        });
    });
    req.on('error', (e) => {
        console.error(e);
    });
    req.write(postData);
    req.end();
}

let dsmLogout = (sid) => {
    let postData = QueryString.stringify({
        "sID": sid
    })
    let options = {
        hostname: TARGET_DSM,
        port: 443,
        path: '/authentication/logout',
        method: 'DELETE',
        // headers: {
        //      'Content-Type': 'application/json'
        //    }
    }
    let req = HTTPS.request(options, (res) => {
      
    });
    req.write(postData);
    req.end();
}

var port = process.env.BINARY_SERVER_PORT || 41118
binaryServer = BinaryServer({port});
binaryServer.on('connection', function(client) {
    console.log('new connection')
    let randomFileName = UPLOAD_FOLDER + Math.random().toString(36).substring(2) + '.wav'
    var fileWriter = new wav.FileWriter(randomFileName, {
        channels: 1,
        sampleRate: 48000,
        bitDepth: 16
    })
  
    client.on('stream', function(stream, meta) {
        console.log('new stream')
        stream.pipe(fileWriter)
    
        stream.on('end', function() {
            fileWriter.end()
            Recognize.syncRecognize(randomFileName, 'LINEAR16', 48000, 'en-US', function(data) {
                data = data.toLowerCase()
                if (data === '')
                    data = 'Cannot recognize'
                let res = {
                    recognizeResult: data
                }
                if (data.includes("open") && data.includes("computer")) {
                    console.log('Action: open computers')
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#computers_root'
                    stream.write(JSON.stringify(res))

                } else if (data.includes("create") && data.includes("group")) {
                    console.log('Action: create a relay group')
                    let sid = dsmLogin((sid) => {
                        let postData = JSON.stringify({"CreateRelayGroupRequest": {}})
                        let options = {
                            hostname: TARGET_DSM,
                            port: 443,
                            path: '/rest/relay-groups',
                            method: 'POST',
                            headers: {
                                    'Content-Type': 'application/json',
                                    'Cookie': 'sID=' + sid
                               }
                        }
                        let req = HTTPS.request(options, (res) => {
                          
                            res.on('data', (d) => {
                                console.log('create relay group success')
                            });
                        });
                        req.on('error', (e) => {
                            console.error(e);
                        })
                        req.write(postData);
                        req.end();
                        dsmLogout(sid)

                        res.openWindow = TARGET_DSM_FULL + 'Application.screen?#administration_relayLists'
                        stream.write(JSON.stringify(res))
                    })
                } else if (data.includes('open') && data.includes('administration')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_systemSettings'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('system event')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_systemEvents'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('antimalware event')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_antiMalwareEvents'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('web reputation event')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_webReputationEvents'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('firewall event')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_firewallEvents'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('intrusion prevention event')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_deepPacketInspectionEvents'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('integrity event')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_integrityEvents'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('log inspection event')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_logAnalysisEvents'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('app control event')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_appControlSecurityEvents'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('generate report')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#eventsAndReports_reports'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('policy') || data.includes('open') && data.includes('policies')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_policies'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('firewall rule')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_firewallRules'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('intrusion prevention rule')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_deepPacketInspectionRules'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('integrity monitoring rule')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_integrityRules'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('log inspection rule')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_logInspectionRules'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('app control rule')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_appControlRulesets'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('directory list')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_scanDirectoryLists'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('file extension list')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_scanFileExtLists'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('file list')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_scanFileLists'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('ip list')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_iPLists'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('mac list')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_mACLists'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('port list')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_portList'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('context')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_ruleContexts'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('firewall stateful configuration')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_statefulConfigurations'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('malware scan configuration')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_antiMalwareConfigs'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('schedules')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_schedules'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('syslog configuration')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_syslog'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('tag')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#policies_tags'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('system settings')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_systemSettings'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('scheduled task')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_scheduledTasks'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('event based task')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_eventBasedTasks'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('user')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_users'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('role')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_roles'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('contact')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_contacts'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('security rule')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_ruleUpdates'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('security pattern')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_patternUpdates'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('software')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_localSoftware'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('relay management')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#administration_relayLists'
                    stream.write(JSON.stringify(res))
                } else if (data.includes('open') && data.includes('alert')) {
                    res.redirect = TARGET_DSM_FULL + 'Application.screen?#alerts'
                    stream.write(JSON.stringify(res))
                } else {
                    stream.write(JSON.stringify(res))
                }
            });
            console.log('wrote to file ' + randomFileName)
        })
    })
})

console.log('server started.')