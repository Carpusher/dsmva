# Deep Security Manager Voice Assistant
[Demo Site](dsmva-193801.appspot.com) (Require DSaas staging account.)

## Dev Requirement
### Prepare GCP Speech API
Follow the [guide](https://github.com/googleapis/nodejs-speech/blob/master/README.md#before-you-begin) to create a GCP project, enable Speech API, and create a service account.
### Install Packages
```
npm i
npm install -g nodemon
```
## Run Dev
- Front end

```
npm run dev
```
- Voice streaming server

```
npm run dev-bin
```

## Thanks To
- [cwilso/AudioRecorder](https://github.com/cwilso/AudioRecorder)
- [gabrielpoca/browser-pcm-stream](https://github.com/gabrielpoca/browser-pcm-stream)
